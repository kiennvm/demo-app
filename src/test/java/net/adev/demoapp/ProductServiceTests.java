package net.adev.demoapp;

import net.adev.demoapp.model.entity.Product;
import net.adev.demoapp.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@Testcontainers
@ActiveProfiles("test")
class ProductServiceTests {

    @Autowired
    private ProductRepository productRepository;

    @Test
    void shouldReturnAlLastNames() {
        Product entity = new Product();
        productRepository.saveAndFlush(entity);

        assertEquals(1, entity.getId());
    }
}
