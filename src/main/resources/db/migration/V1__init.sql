create table product
(
    id          bigint not null auto_increment,
    code        varchar(255),
    primary key (id)
) engine = InnoDB;