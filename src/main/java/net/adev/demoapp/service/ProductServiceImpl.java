package net.adev.demoapp.service;

import net.adev.demoapp.model.entity.Product;
import net.adev.demoapp.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public long createProduct() {
        var order = Product.builder().code(UUID.randomUUID().toString()).build();
        return productRepository.save(order).getId();
    }

    @Override
    public List<Product> getProducts(int page, int size) {
        return productRepository.findAll(PageRequest.of(page - 1, size)).getContent();
    }
}
