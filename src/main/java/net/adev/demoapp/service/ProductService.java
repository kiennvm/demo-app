package net.adev.demoapp.service;

import net.adev.demoapp.model.entity.Product;

import java.util.List;

public interface ProductService {
    long createProduct();

    List<Product> getProducts(int page, int size);
}
