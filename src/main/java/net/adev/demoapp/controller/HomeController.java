package net.adev.demoapp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
@Slf4j
public class HomeController {

    @GetMapping("/greeting")
    public ResponseEntity<?> greeting() {
        var msg = "Hello from Spring Boot Application";
        return ResponseEntity.ok(msg);
    }
}
