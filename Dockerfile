FROM openjdk:11-ea-11-jdk-slim
ADD target/*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]